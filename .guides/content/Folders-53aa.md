You can think of a *folder*, also known as a *directory*, as a filing drawer. It contains one or more files.

![](.guides/img/folders.png)

Notice the one labeled **Root** at the top. This is the *root folder* and is the topmost folders. 

Folders can contain files, but they can also contain other folders.

To create a new folder

1. Right click on the folders where you want to create your new folder and select 'New Folder'.
1. Enter the folder name you want to create. 
1. When naming files and folders, only use letters and numbers and avoid any spaces.
1. Avoid using upper case (capitals) unless you have a good reason, which you don't right now.
1. When you press Enter or the 'OK' button, you should see your new folder in the file tree on the left.



